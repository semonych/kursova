-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 18, 2017 at 12:36 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `online_booking`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2017_03_31_140344_ChangeUsersTable', 2),
('2017_04_06_123144_CreateRoomTable', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `room_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `image`, `room_id`, `created_at`, `updated_at`) VALUES
(18, '1493826628G0010078.JPG', 2, '2017-05-03 12:50:28', '2017-05-03 12:50:28'),
(19, '1493826628G0020079.JPG', 2, '2017-05-03 12:50:28', '2017-05-03 12:50:28'),
(20, '1493826628G0030082.JPG', 2, '2017-05-03 12:50:28', '2017-05-03 12:50:28'),
(21, '1493826782G0818977.JPG', 1, '2017-05-03 12:53:02', '2017-05-03 12:53:02'),
(22, '1493826783G0818979.JPG', 1, '2017-05-03 12:53:03', '2017-05-03 12:53:03'),
(23, '1493826783G0818980.JPG', 1, '2017-05-03 12:53:03', '2017-05-03 12:53:03'),
(25, '14938344790005_1445fa8e44.jpg', 3, '2017-05-03 15:01:19', '2017-05-03 15:01:19'),
(26, '14938344790028_c5c20d826f.jpg', 3, '2017-05-03 15:01:19', '2017-05-03 15:01:19'),
(27, '14938346850009_12eeaf1c70.jpg', 4, '2017-05-03 15:04:45', '2017-05-03 15:04:45'),
(28, '14938346850010_e94fbbf6bd.jpg', 4, '2017-05-03 15:04:45', '2017-05-03 15:04:45'),
(29, '14938346850011.jpg', 4, '2017-05-03 15:04:45', '2017-05-03 15:04:45'),
(31, '1493834685deluxe1_ea8275d49a.jpg', 4, '2017-05-03 15:04:45', '2017-05-03 15:04:45'),
(32, '1493834685deluxe2_80c67383d0.jpg', 4, '2017-05-03 15:04:45', '2017-05-03 15:04:45'),
(33, '14938348140015_217749d10c.jpg', 5, '2017-05-03 15:06:54', '2017-05-03 15:06:54'),
(34, '14938348150016_bfd3d58e7d.jpg', 5, '2017-05-03 15:06:55', '2017-05-03 15:06:55'),
(35, '14938348150017_65c775d982.jpg', 5, '2017-05-03 15:06:55', '2017-05-03 15:06:55'),
(36, '14938348150027_5567a921e5.jpg', 5, '2017-05-03 15:06:55', '2017-05-03 15:06:55'),
(37, '14938348150048_6093e1b5ac.jpg', 5, '2017-05-03 15:06:55', '2017-05-03 15:06:55'),
(38, '14938348150049_a89008a6a1.jpg', 5, '2017-05-03 15:06:55', '2017-05-03 15:06:55'),
(39, '14938350130004_42b968a263.jpg', 6, '2017-05-03 15:10:13', '2017-05-03 15:10:13'),
(40, '14938350130015_217749d10c.jpg', 6, '2017-05-03 15:10:13', '2017-05-03 15:10:13'),
(41, '14938350130018_384c414b71.jpg', 6, '2017-05-03 15:10:13', '2017-05-03 15:10:13'),
(42, '14938350130019_c98e3325fa.jpg', 6, '2017-05-03 15:10:13', '2017-05-03 15:10:13'),
(43, '14938350130020_b3737104b1.jpg', 6, '2017-05-03 15:10:13', '2017-05-03 15:10:13'),
(44, '14938350130021_722047e337.jpg', 6, '2017-05-03 15:10:13', '2017-05-03 15:10:13'),
(45, '14938350130022_460c120893.jpg', 6, '2017-05-03 15:10:13', '2017-05-03 15:10:13'),
(46, '14938350130023_c74cbb8b42.jpg', 6, '2017-05-03 15:10:13', '2017-05-03 15:10:13'),
(47, '14938350130024_67d75508bf.jpg', 6, '2017-05-03 15:10:13', '2017-05-03 15:10:13'),
(48, '14938350130025_85e5b3b2c5.jpg', 6, '2017-05-03 15:10:13', '2017-05-03 15:10:13'),
(49, '14938350130048_6093e1b5ac.jpg', 6, '2017-05-03 15:10:13', '2017-05-03 15:10:13');

-- --------------------------------------------------------

--
-- Table structure for table `reserves`
--

CREATE TABLE IF NOT EXISTS `reserves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_room` varchar(255) NOT NULL,
  `date` text NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `reserves`
--

INSERT INTO `reserves` (`id`, `id_room`, `date`, `updated_at`, `created_at`, `user_id`) VALUES
(8, '1', '''2017-05-22'',''2017-05-23'',', '2017-04-18 07:37:19', '2017-04-18 07:37:19', 28),
(9, '1', '''2017-05-25'',''2017-05-26'',''2017-05-27'',', '2017-04-28 10:43:22', '2017-04-28 10:43:22', 28),
(10, '6', '''2017-06-14'',''2017-06-15'',''2017-06-16'',', '2017-05-03 15:11:54', '2017-05-03 15:11:54', 28);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `room_price` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  `bed` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `room_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room_name`, `room_price`, `area`, `bed`, `description`, `room_image`, `created_at`, `updated_at`) VALUES
(3, 'Стандарт', 950, 20, 'Двоспальне ліжко', '<h3>Кімната</h3>\r\n\r\n<ul>\r\n	<li>Ковдри та подушки з гусячого пуху</li>\r\n	<li>Високоякісна постільна білизна з 100% бавовни</li>\r\n	<li>Просторий душ або ванна</li>\r\n	<li>Безкоштовний бездротовий Інтернет (Wi-Fi)</li>\r\n	<li>Гіпоалергенні ковдри та подушки на вимогу</li>\r\n	<li>HD телевізор з 32 дюймовим екраном</li>\r\n	<li>Система кондиціонування / опалення</li>\r\n	<li>Робоча зона</li>\r\n	<li>Міні-бар та електронний сейф</li>\r\n</ul>\r\n\r\n<h3>В ванній кімнаті</h3>\r\n\r\n<ul>\r\n	<li>Душ або ванна</li>\r\n	<li>Халати з 100% бавовни</li>\r\n	<li>Рушники та тапочки з 100% бавовни</li>\r\n	<li>Фен та збільшуюче дзеркало</li>\r\n</ul>\r\n\r\n<h3>Послуги</h3>\r\n\r\n<ul>\r\n	<li>Обслуговування в номері</li>\r\n	<li>Масаж в номері</li>\r\n	<li>Послуги прання, хімчистки та прасування</li>\r\n</ul>\r\n', '14938344110005.jpg', '2017-05-03 15:00:11', '2017-05-03 15:00:11'),
(4, 'Півлюкс', 1100, 27, 'Двоспальне ліжко', '<h3>Кімната</h3>\r\n\r\n<ul>\r\n	<li>Ковдри та подушки з гусячого пуху</li>\r\n	<li>Високоякісна постільна білизна з 100% бавовни</li>\r\n	<li>Просторий душ або/та ванна</li>\r\n	<li>Безкоштовний бездротовий Інтернет (Wi-Fi)</li>\r\n	<li>Гіпоалергенні ковдри та подушки на вимогу</li>\r\n	<li>Додаткові штори (black outs) на вікнах</li>\r\n	<li>HD телевізор з 32 дюймовим екраном</li>\r\n	<li>Система кондиціонування / опалення</li>\r\n	<li>Робоча зона</li>\r\n	<li>Зручна зона для відпочинку з м&#39;яким кріслом або диваном</li>\r\n	<li>Міні бар та електронний сейф</li>\r\n	<li>Суміжні кімнати</li>\r\n	<li>Кімнати підлаштовані для інвалідних візків</li>\r\n	<li>Дитячі/додаткові ліжка</li>\r\n</ul>\r\n\r\n<h3>В ванній кімнаті</h3>\r\n\r\n<ul>\r\n	<li>Душ або/та ванна</li>\r\n	<li>Халати з 100% бавовни</li>\r\n	<li>Рушники та тапочки з 100% бавовни</li>\r\n	<li>Фен та збільшуюче дзеркало</li>\r\n</ul>\r\n\r\n<h3>Послуги</h3>\r\n\r\n<ul>\r\n	<li>Обслуговування в номері</li>\r\n	<li>Масаж в номері</li>\r\n	<li>Послуги прання, хімчистки та прасування</li>\r\n</ul>\r\n', '14938346720009_12eeaf1c70.jpg', '2017-05-03 15:04:32', '2017-05-03 15:04:32'),
(5, 'Люкс', 1500, 50, 'Двоспальне ліжко + Диван', '<h3>Кімната</h3>\r\n\r\n<ul>\r\n	<li>Ковдри та подушки з гусячого пуху</li>\r\n	<li>Високоякісна постільна білизна з 100% бавовни</li>\r\n	<li>Просторий душ або/та ванна</li>\r\n	<li>Безкоштовний бездротовий Інтернет (Wi-Fi)</li>\r\n	<li>Гіпоалергенні ковдри та подушки на вимогу</li>\r\n	<li>Додаткові штори (black outs) на вікнах</li>\r\n	<li>HD телевізор з 32 дюймовим екраном</li>\r\n	<li>Система кондиціонування / опалення</li>\r\n	<li>Робоча зона</li>\r\n	<li>Зручна зона для відпочинку з м&#39;яким кріслом або диваном</li>\r\n	<li>Міні бар та електронний сейф</li>\r\n	<li>Суміжні кімнати</li>\r\n	<li>Кімнати підлаштовані для інвалідних візків</li>\r\n	<li>Дитячі/додаткові ліжка</li>\r\n</ul>\r\n\r\n<h3>В ванній кімнаті</h3>\r\n\r\n<ul>\r\n	<li>Душ або/та ванна</li>\r\n	<li>Халати з 100% бавовни</li>\r\n	<li>Рушники та тапочки з 100% бавовни</li>\r\n	<li>Фен та збільшуюче дзеркало</li>\r\n</ul>\r\n\r\n<h3>Послуги</h3>\r\n\r\n<ul>\r\n	<li>Обслуговування в номері</li>\r\n	<li>Масаж в номері</li>\r\n	<li>Послуги прання, хімчистки та прасування</li>\r\n</ul>\r\n', '14938348000016_bfd3d58e7d.jpg', '2017-05-03 15:06:40', '2017-05-03 15:06:40'),
(6, 'Представницький Люкс', 3500, 75, 'Велике двоспальне ліжко', '<h3>Кімната</h3>\r\n\r\n<ul>\r\n	<li>Ковдри та подушки з гусячого пуху</li>\r\n	<li>Високоякісна постільна білизна з 100% бавовни</li>\r\n	<li>Просторий душ або/та ванна</li>\r\n	<li>Безкоштовний бездротовий Інтернет (Wi-Fi)</li>\r\n	<li>Гіпоалергенні ковдри та подушки на вимогу</li>\r\n	<li>Додаткові штори (black outs) на вікнах</li>\r\n	<li>HD телевізор з 32 дюймовим екраном</li>\r\n	<li>Система кондиціонування / опалення</li>\r\n	<li>Робоча зона</li>\r\n	<li>Зручна зона для відпочинку з м&#39;яким кріслом або диваном</li>\r\n	<li>Міні бар та електронний сейф</li>\r\n	<li>Суміжні кімнати</li>\r\n	<li>Кімнати, підлаштовані для інвалідних візків</li>\r\n	<li>Дитячі/додаткові ліжка</li>\r\n</ul>\r\n\r\n<h3>В ванній кімнаті</h3>\r\n\r\n<ul>\r\n	<li>Душ або/та ванна</li>\r\n	<li>Халати з 100% бавовни</li>\r\n	<li>Рушники та тапочки з 100% бавовни</li>\r\n	<li>Фен та збільшуюче дзеркало</li>\r\n</ul>\r\n\r\n<h3>Послуги</h3>\r\n\r\n<ul>\r\n	<li>Обслуговування в номері</li>\r\n	<li>Масаж в номері</li>\r\n	<li>Послуги прання, хімчистки та прасування</li>\r\n</ul>\r\n', '14938349960004_42b968a263.jpg', '2017-05-03 15:09:56', '2017-05-03 15:09:56');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `testimonials` text NOT NULL,
  `name_user` varchar(255) NOT NULL,
  `room_id` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `testimonials`, `name_user`, `room_id`, `updated_at`, `created_at`) VALUES
(8, 'Дуже класно', 'Andre', 4, '2017-05-03 15:13:45', '2017-05-03 15:13:45');

-- --------------------------------------------------------

--
-- Table structure for table `tidings`
--

CREATE TABLE IF NOT EXISTS `tidings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tidings`
--

INSERT INTO `tidings` (`id`, `title`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Новий рік 2017', 'news1.png', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', '2017-04-26 11:53:00', '2017-04-26 11:53:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `login`, `avatar`, `role`) VALUES
(28, 'Andre', 'semaandco@gmail.com', '$2y$10$OAl4i8ZP20bjSYJ.d2hmIOCv/vhpud6UrEzfbKU4Hte3oBuJvdTBS', 'WyD6r93OCo7hwVlZIEC51V4wWxpv2yVfWDDBQQ8V3M9iPHilfLSCQebfNlFP', '2017-03-31 12:05:39', '2017-04-28 11:31:26', 'semonych', 'user-icon.png', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
