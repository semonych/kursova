@extends('layouts.layouts')
@section('content')
<div class="content">
    <div class="container">
      <div class="row mt50 mb20">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#home">Мої дані</a></li>
          <li><a data-toggle="tab" href="#menu1">@if(Auth::user()->role === 'admin') Бронювання @else Мої бронювання @endif</a></li>
          @if(Auth::user()->role === 'admin')
          <li><a data-toggle="tab" href="#menu2">Відгуки</a></li>
          <li><a data-toggle="tab" href="#menu3">Новини</a></li>
          <li><a data-toggle="tab" href="#menu4">Номера</a></li>
          @endif
        </ul>
        <div class="tab-content mt20">
          <div id="home" class="tab-pane fade in active">
            <div class="col-md-3">
              <div class="input-group">
                <label for="">Name</label>
                <input type="text" class="form-control" placeholder="name" value="{{Auth::user()->name}}" >
              </div>
            </div>
            <div class="col-md-3">
              <div class="input-group">
                <label for="">Email</label>
                <input type="email" class="form-control" placeholder="email" value="{{Auth::user()->email}}" >
              </div>
            </div>
            <div class="col-md-3">
              <div class="input-group">
                <label for="">Login</label>
                <input type="text" class="form-control" placeholder="login" value="{{Auth::user()->login}}" >
              </div>
            </div>
            <div class="col-md-3">
              <div class="input-group">
                <label for=""> New Password</label>
                <input type="password" class="form-control" placeholder="password" value="" >
              </div>
            </div>

          </div>
          <div id="menu1" class="tab-pane fade">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Id reserve</th>
                  <th>Id room</th>
                  <th>Date</th>
                  @if(Auth::user()->role === 'admin')
                  <th>User id</th>
                  <th>Delete</th>
                  @endif
                </tr>
              </thead>
              <tbody>
                @foreach($reserves as $reserve)
                <tr>
                  <td>{{$reserve->id}}</td>
                  <td><a href="/room/{{$reserve->id_room}}">{{$reserve->id_room}}</a></td>
                  <td>
                    <?php $date=str_replace("'","",$reserve->date);
                    echo $date; ?>
                  </td>
                  @if(Auth::user()->role === 'admin')
                  <td>{{$reserve->user_id}}</td>
                  <td><a class="btn btn-danger" href="/delete-reserve/{{$reserve->id}}">Видалити</a></td>
                  @endif
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @if(Auth::user()->role === 'admin')
          <div id="menu2" class="tab-pane fade">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Description</th>
                  <th>Name User</th>
                  <th>room id</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                @foreach($testimonials as $testimonial)
                <tr>
                  <td>{{$testimonial->id}}</td>
                  <td>{{$testimonial->testimonials}}</td>
                  <td>{{$testimonial->name_user}}</td>
                  <td>{{$testimonial->room_id}}</td>
                  <td><a class="btn btn-danger" href="/delete-testimonial/{{$testimonial->id}}">Видалити</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <div id="menu3" class="tab-pane fade">
            <a class=" btn btn-success" href="add-news">Add news</a>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>title</th>
                  <th>Image</th>
                  <th>Descritpion</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                @foreach($news as $new)
                <tr>
                  <td>{{$new->id}}</td>
                  <td>{{$new->title}}</td>
                  <td><img src="images/upload/{{$new->image}}" alt=""></td>
                  <td>{{$new->description}}</td>
                  <td><a class="btn btn-danger" href="/delete-news/{{$new->id}}">Видалити</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <div id="menu4" class="tab-pane fade">
            <a class=" btn btn-success" href="/room/add">Add room</a>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Room Name</th>
                  <th>Image</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                @foreach($rooms as $room)
                <tr>
                  <td>{{$room->id}}</td>
                  <td>{{$room->room_name}}</td>
                  <td><img src="images/upload/{{$room->room_image}}" style="width:80px;" alt=""></td>
                  <td><a class="btn btn-warning" href="/room/edit/{{$room->id}}">Редагувати</a></td>
                  <td><a class="btn btn-danger" href="/room/delete/{{$room->id}}">Видалити</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>
@endsection
