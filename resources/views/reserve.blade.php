@extends('layouts.layouts')
@section('content')
  <div class="content">
    <div class="container">
      <hr>
      <div class="row">
        @if (session('status'))
            <div class="alert alert-danger">
                {{ session('status') }}
            </div>
        @endif
        <div class="col-md-12 center mb20">
          <div class="intro">
            <p class="unavailable-block"></p><span> - не доступно</span>
            <p class="available-block"></p><span> - вибрані</span>
          </div>
          <div id="custom-first-day" data-toggle="calendar"></div>
          <form class="form-reserve" action="/reserve" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="room_id" value="{{$room_id}}">
            <div class="input-group">
            </div>
            <div class="button-group">
            </div>
          </form>

        </div>
        <div class="col-xss-8">
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/dateTimePicker.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
      $('#custom-first-day').calendar(
      {
        day_first: 1,
        num_next_month: 2,
        day_name: ['Нд', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        month_name: ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
        unavailable: [<?=$day ?>],
        onSelectDate: function(date, month, year){
          window._date=[year, month, date].join('-');
        }
      });
      $(document).ready(function(){
        $(document).on('click','.available.cur-month',function() {
          if(!document.getElementById('button')){
            var element = ` <button type = 'submit' id="button" name="button">Забронювати</button> `;
            $('.button-group').append(element);
          }
          if($(this).hasClass('select_date')){
            $(this).removeClass('select_date');
            document.getElementById(_date).remove();
          }
          else{
            if (!$(this).hasClass('near-month')) {
              $( this ).addClass('select_date');
              if(!document.getElementById(window._date)){
                var element = ` <input type="hidden" name="text[]" value="${window._date}" id="${window._date}" ></input> `;
                $('.input-group').append(element);
              }
            }


          }
        });
      });
    });


    </script>
@endsection
