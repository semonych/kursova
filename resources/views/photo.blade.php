@extends('layouts.layouts')
@section('content')
<div class="content">
    <div class="container">
      <div class="row mt50 mb20">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <a class="btn btn-success" href="/room/edit/{{$id}}">Назад</a>
        <a class="btn btn-success" href="photo/add" >Додати</a>
        <table class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Image</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            @foreach($photos as $photo)
            <tr>
              <td>{{$photo->id}}</td>
              <td><img src="/images/upload/{{$photo->image}}"  style="width:100px;"alt=""></td>
              <td><a class="btn btn-danger" href="photo/delete/{{$photo->id}}">Видалити</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
