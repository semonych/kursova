@extends('layouts.layouts')
@section('content')
<div class="content">
    <div class="container">
      <div class="row mt50 mb20">
        <div class="col-md-12">
          <form class="" action="add-news" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
              <label for="title">Заголовок</label>
              <input type="text" class="form-control" id="title" name="title" required>
            </div>
            <div class="form-group">
              <label for="pwd">Зображення:</label>
              <input type="file" class="form-control" id="pwd" name="image" required>
            </div>
            <div class="form-group">
              <label for="pwd">Опис:</label>
              <textarea name="description" class="form-control" rows="8" cols="80" required></textarea>
            </div>
            <button type="submit" class="btn btn-default">Додати</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
