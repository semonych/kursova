@extends('layouts.layouts')
@section('content')
  <div class="content">
    <div class="img-wrap">
      <img src="images/pic.png" alt="">
      <div class="form">
        <div class="form-content">
          <div class="icon">
            <img src="images/user.png" alt="">
          </div>
          <div class="form-title">
            <p>Реєстрація</p>
          </div>
          <div class="form-input">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{url('/register')}}" method="POST" autocomplete="off">
              {{csrf_field()}}
              <input type="text" name="name" placeholder="Ім'я та Прізвище">
              <input type="email" name="email" placeholder="Ваша електронна адресса">
              <input type="password" name="password" placeholder="Пароль">
              <input type="text" name="login" placeholder="Ваш логін">
              <button type="submit"  class="form-button" name="button">Розпочати</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
