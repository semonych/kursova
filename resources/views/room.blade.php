@extends('layouts.layouts')
@section('content')
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          @if (session('status'))
              <div class="alert alert-success">
                  {{ session('status') }}
              </div>
          @endif
          <div class="room-content">
            <p>{{$rooms->room_name}}</p>
            @if (Auth::check())
              <a href="/reserve/{{$rooms->id}}">
                <div class="room-button right">
                  Забронювати
                </div>
              </a>
              @endif

              <div>
                <img id="largeImg" src="/images/upload/{{$rooms->room_image}}" alt="Large image">
                <div id="thumbs">
                  <div class="row">
                  @if($photos)
                    @foreach($photos as $photo)
                      <div class="col-md-2 nopadding">
                        <a href="/images/upload/{{$photo->image}}" ><img src="/images/upload/{{$photo->image}}"></a>
                      </div>
                    @endforeach
                  @endif
                  </div>
                </div>
              </div>
              <div class="content-title">
                <span>Детальний опис</span>
              </div>
              <p class="desc-room">
                {!!$rooms->description!!}
                Вартість номера<br>
                <strong>{{$rooms->room_price}} грн./доба </strong>
              </p>
              @if(count($testimonials)>0)
                <div class="content-title mb20">
                  <span>Останні відгуки</span>
                </div>
                @foreach($testimonials as $testimonial)
                  <div class="room-testimonials">
                    <p></p>
                    <p class="blue"><span class="glyphicon glyphicon-user " aria-hidden="true"></span>{{$testimonial->name_user}} <span class=" right glyphicon glyphicon-calendar" aria-hidden="true"><span>{{$testimonial->created_at->format('d.m.Y')}}</span></span></p>
                    <p>{{$testimonial->testimonials}}</p>
                  </div>
                @endforeach
              @endif
              @if (Auth::check())
              <form class="form-testimonials" action="/add-testimonials" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="content-title" style="padding-bottom: 40px;">
                  <span>Залиште свій відгук</span>
                </div>
                <div class=" input-group">
                  <textarea  required class="form-control" name="testimonials" rows="8" cols="80" placeholder="Залиште будь-ласка свій відгук"></textarea>
                </div>
                <div class=" input-group" style="padding-top: 15px;">
                  <input type="hidden" name="roomId" value="{{$rooms->id}}">
                <button type="submit" class="btn btn-default"name="button">Надіслати</button>
                </div>
              </form>
              @endif
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
