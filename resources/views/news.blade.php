@extends('layouts.layouts')
@section('content')
<div class="content">
    <div class="container">
      <div class="row mt50 mb20">
        @if(count($news)>0)
        @foreach($news as $new)
        <div class="col-md-4">
          <div class="news-wrap">
            <p>{{$new->title}}</p>
            <span class="glyphicon glyphicon-calendar center" aria-hidden="true"><span>{{$new->created_at->format('d.m.Y')}}</span></span>
            <img src="images/upload/{{$new->image}}" alt="">
            <p class="last-news-p">{{$new->description}}</p>
            <a href="news/{{$new->id}}">
              <div class="news-button">
                Детальніше
              </div>
            </a>
          </div>
        </div>
        @endforeach
        @endif
      </div>
    </div>
  </div>
@endsection
