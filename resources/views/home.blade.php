@extends('layouts.layouts')

@section('content')
<div class="content">
  <img src="images/pic.png" alt="">
  <div class="container mb20">
    <div class="row">
      <div class="content-title">
        <span>Наші номера</span>
      </div>
      @foreach($rooms as $room)
      <div class="col-md-6">
        <div class="row row-room">
          <div class="col-md-6 room-img" style="background: url(../images/upload/{{$room->room_image}}) no-repeat; background-size: cover;">
          </div>
          <div class="col-md-6 room">
            <p>{{$room->room_name}}</p>
            <div class="content-room">
              <p><span class="glyphicon glyphicon-fullscreen"></span>{{$room->area}} кв.м</p>
              <p><span class="glyphicon glyphicon-bed"></span>{{$room->bed}}</p>
              <p>
                <span class="glyphicon glyphicon-comment"></span>4 відгуків</p>
              <p class="last">Вартість: <span class="blue"></span> <span class="yellow"> {{$room->room_price}} грн</span></p>
              <div class="detail-wrap">
                <a class="btn btn-more" href="/room/{{$room->id}}" role="button">
                  Детальніше <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endforeach
      @if(count($testimonials)>0)
      <div class="col-md-6">
        <div class="last-testimonials">
          <div class="content-title">
            <span>Останній відгук</span>
          </div>
          @foreach($testimonials as $testimonial)
          <div class="row row-room testimonials">
            <p></p>
            <p class="blue"><span class="glyphicon glyphicon-user " aria-hidden="true"></span>{{$testimonial->name_user}} <span class=" right glyphicon glyphicon-calendar" aria-hidden="true"><span>{{$testimonial->created_at->format('d.m.Y')}}</span></span></p>
            <p>{{$testimonial->testimonials}}</p>
          </div>
          @endforeach
        </div>
      </div>
      @endif
      </div>
    </div>
  </div>
@endsection
