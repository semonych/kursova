<?php

namespace App\Http\Controllers;
use App\Testimonial;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;

class AddTestimonialsController extends Controller
{
    //
    public function __construct()
    {
      $this->middleware('auth');
    }
    public function getTestimonial(Request $request)
    {
      $roomId=$request->input('roomId');
      $testimonials=$request->input('testimonials');
      $articles=new Testimonial;
      $articles->testimonials=$testimonials;
      $articles->name_user=Auth::user()->name;
      $articles->room_id=$roomId;
      $articles->save();
      return redirect('room/'.$roomId)->with('status', 'Відгук залишено успішно.Дякуємо вам!');
    }
}
