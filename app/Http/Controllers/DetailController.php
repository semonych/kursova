<?php

namespace App\Http\Controllers;
use Auth;
use App\Reserve;
use App\Tiding;
use App\Room;
use App\Testimonial;
use Illuminate\Http\Request;

use App\Http\Requests;

class DetailController extends Controller
{
    //
    public function __construct()
    {
      $this->middleware('auth');
    }
    public function deleteReserve($id)
    {
      # code...
      if(Auth::user()->role === 'admin')
      {
       Reserve::destroy($id);
       return redirect('my-details')->with('status', 'Резервацію видалено успішно');
     }
    }
    public function deleteTestimonial($id)
    {
      # code...
      if(Auth::user()->role === 'admin')
      {
       Testimonial::destroy($id);
       return redirect('my-details')->with('status', 'Відгук видалено успішно');
     }
    }

    public function myDetails()
    {
      if (Auth::check()) {
        if(Auth::user()->role === 'admin')
        {
          $reserves = Reserve::orderBy('id','desc')->get();
          $testimonials=Testimonial::all();
          $news=Tiding::all();
          $rooms=Room::orderBy('id','desc')->get();
        }
        else{
          $reserves=Reserve::where('user_id',Auth::id())->get();
          $testimonials='';
        }
        return view('my-details')->with(['title'=>'Мої дані','reserves'=>$reserves,'testimonials'=>$testimonials,'news'=>$news,'rooms'=>$rooms]);
      }
      else{
        return view('home');
      }
    }
}
