<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Reserve;
use App\Http\Requests;

class ReserveController extends Controller
{
    //
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function getReserve(Request $request)
    {
      $roomId=$request->input('room_id');
      $dates=$request->input('text');
      $reserveDate='';
      if ($dates) {
      foreach ($dates as $date) {
        $d1 = strtotime($date); // переводит из строки в дату
        $date2 = date("Y-m-d", $d1); // переводит в новый формат
        $reserveDate.="'".$date2."',";
      }
      $articles=new Reserve;
      $articles->id_room=$roomId;
      $articles->date=$reserveDate;
      $articles->user_id=Auth::id();
      $articles->save();
      return redirect('my-details')->with('status', 'Номер зарезервовано успішно. Перейдіть в "Мої бронювання" для перегляду вашої резервації');
      }
      return back()->with('status','Оберіть хоча б одну дату');
    }
    public function showReserve(Request $request, $id)
    {
      $day='';
      for ($i=1; $i <60 ; $i++) {
        # code...
        $time = strtotime("-".$i." day");
        $fecha = date("Y-m-d", $time);
        $day.="'".$fecha."',";
      }
      $articles=Reserve::where('id_room',$id)->get();
      foreach ($articles as $article) {
       $day.=$article->date;
     }

      return view('reserve')->with(['title'=>'Бронювання','day'=>$day,'test'=>$day,'room_id'=>$id]);
    }
}
