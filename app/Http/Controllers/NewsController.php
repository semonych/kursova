<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Tiding;
use App\Http\Requests;

class NewsController extends Controller
{
    //

    public function deleteNews($id)
    {
      # code...
      if(Auth::user()->role === 'admin')
      {
       Tiding::destroy($id);
       return redirect('my-details')->with('status', 'Новина видалено успішно');
     }
     return redirect('/');
    }
    public function getNews(Request $request)
    {
      # code...
      $title=$request->input('title');
      $description=$request->input('description');
      //загрузка файлу
      $test = $_FILES['image']['name'];
      $new_name = date('d-m-Y-H-i-s').'.'.$test;
      $upload = "images/upload/".$new_name;
       move_uploaded_file($_FILES['image']['tmp_name'], $upload);
       $image = $new_name;
         //кінець загрузки файлу
      $tiding=new Tiding;
      $tiding->title=$title;
      $tiding->description=$description;
      $tiding->image=$new_name;
      $tiding->save();
      return redirect('my-details')->with(['title'=>'Moї дані','status'=>'Новина успішно добавлена']);
    }
    public function showNews()
    {
      # code...
      $news = Tiding::orderBy('id','desc')->get();
      return view('news')->with(['title'=>'Новини','news'=>$news]);
    }
    public function showNew($id)
    {
      # code...
      $news = Tiding::where('id',$id)->first();
      if(count($news)>0){
        return view('new')->with(['title'=>'d','news'=>$news]);
      }
      return redirect('news');
    }
    public function addNews()
    {
      # code...
      return view('add-news')->with(['title'=>'Додавання новини']);
    }
}
